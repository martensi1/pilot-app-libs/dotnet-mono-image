ARG docker_dotnet_sdk_version=6.0
FROM mcr.microsoft.com/dotnet/sdk:${docker_dotnet_sdk_version}


# Install .NET sdks
RUN wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb && rm packages-microsoft-prod.deb

RUN apt-get update && \
    apt-get install -y apt-transport-https && \
    apt-get update && \
    apt-get install -y dotnet-sdk-5.0 && \
    apt-get install -y dotnet-sdk-3.1 && \
    apt-get install -y dotnet-runtime-5.0 && \
    apt-get install -y dotnet-runtime-3.1


# Add mono dependencies
RUN apt-get update && \
    apt-get install -y apt-transport-https dirmngr gnupg ca-certificates 
  
# Add mono package repository
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN echo "deb https://download.mono-project.com/repo/debian stable-buster main" | tee /etc/apt/sources.list.d/mono-official-stable.list

# Install mono, msbuild and unzip
RUN apt-get update
RUN apt-get install -y mono-runtime mono-devel msbuild


# Cleanup
RUN rm -rf /var/lib/apt/lists/ && rm -Rf /tmp/*
