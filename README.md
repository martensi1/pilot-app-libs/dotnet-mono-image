# .NET Mono Image

.NET SDK Docker Image with Mono framework installed

Primarly used in build pipelines to build [docfx](https://dotnet.github.io/docfx/) projects.


## License

This repository is licensed with the [MIT](LICENSE) license

## Author

martensi (Simon Mårtensson)
